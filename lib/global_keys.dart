import 'package:flutter/material.dart';

class GlobalKeys {
  static final String serverUrl = "http://82.74.82.114:3000";
  static final navKey = GlobalKey<NavigatorState>();
  static bool showingError = false;
static void showErrorDialog(String message) async{
    if(GlobalKeys.showingError) return;
    
    GlobalKeys.showingError = true;
    await showDialog(
      context: GlobalKeys.navKey.currentState.overlay.context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: 
            BorderRadius.all(Radius.circular(15))),
            content: Container(
              height: MediaQuery.of(GlobalKeys.navKey.currentState.overlay.context).size.height * 0.28,
              child: Column(
                children: <Widget>[
                  Icon(Icons.warning, color: Colors.orange, size: MediaQuery.of(GlobalKeys.navKey.currentState.overlay.context).size.height * 0.15,),
                  Text(message, style: TextStyle(color: Colors.orange, fontSize: 21))
                ],
              ),
            ),
            
        );
      },
    );
    GlobalKeys.showingError = false;
  }
}