import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:client/models/coordinate.dart';
import 'package:client/models/points/goto_location.dart';
import 'package:client/screens/landing_screen.dart';
import 'package:client/screens/users/points/finish_screen.dart';
import 'package:client/screens/users/points/goto_location_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';

import 'package:client/models/points/point.dart';
import 'package:client/models/points/riddle_location.dart';
import 'package:client/screens/users/points/riddle_location_screen.dart';
import 'package:client/screens/users/waiting_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../global_keys.dart';

class UserProvider with ChangeNotifier {
  int pincode;
  String name;
  String password;
  String title = "Waiting for host";
  Point point;
  List<dynamic> users = [];
  
  bool hasStarted = false;
  bool hasEnded = false;
  bool appInit = false;
  Timer timer;

   UserProvider(){
    SharedPreferences.getInstance().then((SharedPreferences prefs) {
      http.post(
        GlobalKeys.serverUrl + "/user/checkpincode",

        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          "HttpHeaders.contentTypeHeader": "application/json"        },
        body: jsonEncode(<String, int>{
          'pincode': prefs.getInt("user_pincode"),
        }),
      ).then((http.Response response) {
          if(jsonDecode(response.body)['result']) {
            this.pincode = prefs.getInt("user_pincode");
            this.name = prefs.getString("user_name");
            this.password = prefs.getString("user_password");
  	        
            appInit = true;
            timer = Timer.periodic(Duration(seconds: 2), (Timer t)  { _getStatus(); });

          }
      });
   });
  }

  leaveScavengerhunt() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    timer.cancel();

    prefs.remove("user_pincode");
    prefs.remove("user_name");
    prefs.remove("user_password");

    pincode = null;
    name = null;
    password = null;
    GlobalKeys.navKey.currentState.pushReplacementNamed(LandingScreen.routeName);
  }
  register(String name, String password) async {
    try {
      final http.Response response = await http.post(
        GlobalKeys.serverUrl + "/user/register",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          "HttpHeaders.contentTypeHeader": "application/json"

        },
        body: jsonEncode(<String, dynamic>{
          'pincode': pincode,
          'name': name,
          'password': password
        }),
      );
      dynamic body = jsonDecode(response.body);
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      if(body['result'] == null) {
        if(body['error']['error_code'] == 1) {
          pincode = null;
          GlobalKeys.showErrorDialog("Unknown pincode");
          notifyListeners();
        }
        if(body['error']['error_code'] == 2) GlobalKeys.showErrorDialog("Name already exists");
        if(body['error']['error_code'] == 12) {
          GlobalKeys.showErrorDialog("Scavengerhunt isn't joinable, ask the organisator for open the scavengehunt");
          pincode = null;
          notifyListeners();
        }

        return false;
      }
      if(body['result']) {
        this.name = name;
        this.password = password;
        GlobalKeys.navKey.currentState.pushReplacementNamed(WaitingScreen.routeName);
        prefs.setInt("user_pincode", pincode);
        prefs.setString("user_name", name);
        prefs.setString("user_password", password);
        timer = Timer.periodic(Duration(seconds: 2), (Timer t)  { _getStatus(); });
        return true;
      }
      GlobalKeys.showErrorDialog("Name already exists");
      return false;
    } on SocketException catch(e) {
      GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
      return false;
    }
  }
  checkPincode(int pincode) async {
    try {
      final http.Response response = await http.post(
        GlobalKeys.serverUrl + "/user/checkpincode",

        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          "HttpHeaders.contentTypeHeader": "application/json"

        },
        body: jsonEncode(<String, int>{
          'pincode': pincode,
        }),
      );
      dynamic body = jsonDecode(response.body);

      if(body['result'] == null) {
        if(body['error']['error_code'] == 1) GlobalKeys.showErrorDialog("Unknown pincode");
        if(body['error']['error_code'] == null) GlobalKeys.showErrorDialog("Unknown error");
              GlobalKeys.showErrorDialog("The server is offline atm, please try again later");

        return false;
      } 
      if(body['result']) {
        this.pincode = pincode;
        notifyListeners();
      }
    } on SocketException catch(e) {
      GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
      return false;
    }
  }
  _getStatus() async{
   try {
    final Geolocator geolocator = Geolocator();
    Position position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

      final http.Response response = await http.post(
        GlobalKeys.serverUrl + "/user/getStatus",

        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          "HttpHeaders.contentTypeHeader": "application/json"

        },
        body: jsonEncode(<String, dynamic>{
          'pincode': pincode,
          'name': name,
          'password': password,
          'input': {
            "longitude": position.longitude,
            "latitude": position.latitude,
          }        }),
      );
      dynamic body = jsonDecode(response.body);

      this.users = body['users'];
      this.users.sort((element, element2) => element2['score'].compareTo(element['score']));
      if(body['hasStarted'] != hasStarted || body['newPoint']) {
          hasStarted = true;

        if(body['point'] != null ) {
        if(body['point']['type'] == 'riddle_location') {
          point = RiddleLocation(title: body['point']['title'], riddle: body['point']['riddle']);
          GlobalKeys.navKey.currentState.pushReplacementNamed(RiddleLocationScreen.routeName);
          return;
        } else if(body['point']['type'] == 'finish') {
          point = null;
          GlobalKeys.navKey.currentState.pushReplacementNamed(FinishScreen.routeName);
          return;
        } else if(body['point']['type'] == 'goto_location') {
          point = GoToLocation(title: body['point']['title'], coordinate: Coordinate(body['point']['coordinate']['latitude'], body['point']['coordinate']['longitude']));
          GlobalKeys.navKey.currentState.pushReplacementNamed(GotoLocationScreen.routeName);
          return;
        } else {
          GlobalKeys.showErrorDialog("Unknown point type");
        }
        } else {
          if(hasStarted) {
            //GlobalKeys.showErrorDialog("Something went wrong");
          }
        }
      } else if(appInit) {
        appInit = false;
        if(!body['hasStarted']) {
          GlobalKeys.navKey.currentState.pushReplacementNamed(WaitingScreen.routeName);
          return;
        }

        if(body['point'] != null ) {
        if(body['point']['type'] == 'riddle_location') {
          point = RiddleLocation(title: body['point']['title'], riddle: body['point']['riddle']);
          GlobalKeys.navKey.currentState.pushReplacementNamed(RiddleLocationScreen.routeName);
          return;
        } else if(body['point']['type'] == 'finish') {
          point = null;
          GlobalKeys.navKey.currentState.pushReplacementNamed(FinishScreen.routeName);
          return;
        } else if(body['point']['type'] == 'goto_location') {
          point = GoToLocation(title: body['point']['title'], coordinate: Coordinate(body['point']['coordinate']['latitude'], body['point']['coordinate']['longitude']));
          GlobalKeys.navKey.currentState.pushReplacementNamed(GotoLocationScreen.routeName);
          return;
        } else {
          GlobalKeys.showErrorDialog("Unknown point type");
        }
        } else {
          if(hasStarted) {
            //GlobalKeys.showErrorDialog("Something went wrong");
          }
        }
      }
      notifyListeners();

     } on SocketException catch(e) {
      GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
    }
  }
}