// import 'dart:convert';
// import 'dart:io';

// import 'package:client/global_keys.dart';
// import 'package:client/models/metadata.dart';
// import 'package:client/models/scavengerhunt_editor.dart';
// import 'package:client/screens/host/host_screen.dart';
// import 'package:client/screens/landing_screen.dart';
// import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';

// class HostHandler {
//   static List<ScavengerhuntEditor> _scavengerhunts = [];
//   static int adminCode;
//   static int pincode;
//   static Metadata metadata= Metadata([], false, 'Waiting for connection');

//   static Future setAppUp() async{
//     final SharedPreferences prefs = await SharedPreferences.getInstance();
//     try {
//      pincode = prefs.getInt("host_pincode");
//      adminCode = prefs.getInt("host_admin_code");

//      if(pincode > 0 && pincode > 0) GlobalKeys.navKey.currentState.pushReplacementNamed(HostScreen.routeName);
//     } on FormatException catch(e) {
//     } on NoSuchMethodError catch(e) {
//     }
//  try {
//       List<String> rawSavedScavengerhunts =  prefs.getStringList("scavengerhunts");

//       for(int i = 0; i < rawSavedScavengerhunts.length; i++) {
//         try {
//           ScavengerhuntEditor edit =  ScavengerhuntEditor.fromEncodedDynamic(rawSavedScavengerhunts[i]);
//           HostHandler._scavengerhunts.add(edit);
//          } on FormatException catch(e) {
//          }
//       }
//     } on FormatException catch(e) {
//     } on NoSuchMethodError catch(e) {
//     }
//   }
//   static stopScavengerhunt() async {
//     final SharedPreferences prefs = await SharedPreferences.getInstance();
//     prefs.remove("host_pincode");
//     prefs.remove("host_admin_code");

//     pincode = null;
//     adminCode = null;

//     GlobalKeys.navKey.currentState.pushReplacementNamed(LandingScreen.routeName);
//   }
//   static addScavengerhunt(ScavengerhuntEditor edit) async{
//     HostHandler._scavengerhunts.add(edit);
//     final SharedPreferences prefs = await SharedPreferences.getInstance();

//     List<String> list = prefs.getStringList("scavengerhunts");
//     if(list == null) list = [];
//     list.add(jsonEncode({
//       'title': edit.title,
//       'points': edit.pointsToMap()
//     }));
//     prefs.setStringList("scavengerhunts", list);
//   }
//   static List<ScavengerhuntEditor> getScavengerhunts() {
//     return HostHandler._scavengerhunts;
//   }
//   static startScavengerHunt() async {
//   try {
//       final http.Response response = await http.post(
//         GlobalKeys.serverUrl + "/admin/startScavengerhunt",
//         headers: <String, String>{
//           'Content-Type': 'application/json; charset=UTF-8',
//         },
//         body: jsonEncode(<String, dynamic>{
//           'admin_code': adminCode,
//           'pincode': pincode,
//         }),
//       );
//       dynamic body = jsonDecode(response.body);

//       GlobalKeys.navKey.currentState.pushReplacementNamed(HostScreen.routeName);
//     } on SocketException catch(e) {
//       GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
//     } on NoSuchMethodError catch(e) {
//       GlobalKeys.showErrorDialog("The server isn't working properly");

//     }
//   }
//   static hostScavengerhunt(ScavengerhuntEditor edit) async {
//     try {
//       final SharedPreferences prefs = await SharedPreferences.getInstance();
//       final http.Response response = await http.post(
//         GlobalKeys.serverUrl + "/admin/register",
//         headers: <String, String>{
//           'Content-Type': 'application/json; charset=UTF-8',
//         },
//         body: jsonEncode(<String, dynamic>{
//           'title': edit.title,
//           'points': edit.pointsToMap()
//         }),
//       );
//       dynamic body = jsonDecode(response.body);
//       if(body['result'] == null) {

//         return;
//       }

//       adminCode = body['result']["admin_code"];
//       pincode = body['result']["pincode"];

//       prefs.setInt("host_admin_code", adminCode);
//       prefs.setInt("host_pincode", pincode);

//       GlobalKeys.navKey.currentState.pushReplacementNamed(HostScreen.routeName);
//     } on SocketException catch(e) {
//       GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
//     } on NoSuchMethodError catch(e) {
//       GlobalKeys.showErrorDialog("The server isn't working properly");

//     }
//   }
// }