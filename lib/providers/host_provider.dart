import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:client/models/coordinate.dart';
import 'package:client/models/points/goto_location_editor.dart';
import 'package:client/models/points/point.dart';
import 'package:client/models/points/riddle_location_editor.dart';
import 'package:client/models/scavengerhunt_editor.dart';
import 'package:client/models/score.dart';
import 'package:client/screens/host/host_screen.dart';
import 'package:client/screens/landing_screen.dart';
import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../global_keys.dart';

class HostProvider with ChangeNotifier {
  int pincode;
  int _adminCode;
  List<dynamic> users = [];
  List<ScavengerhuntEditor> scavengerhunts = [];
  bool hasStarted = false;
  bool hasEnded = false;
  bool joinable = false;

  bool appInit = false;

  Timer timer;
  _DatabaseHandler db = _DatabaseHandler();
  
   HostProvider(){
    SharedPreferences.getInstance().then((SharedPreferences prefs) {

    try {
     pincode = prefs.getInt("host_pincode");
     _adminCode = prefs.getInt("host_admin_code");

     if(pincode > 0 && _adminCode > 0) {
       appInit = true;
       timer = Timer.periodic(Duration(seconds: 3), (Timer t)  { _getStatus(); });
     }

    } on NoSuchMethodError catch(e) {
    }
  });
  _updateScavengerhunts();
  }
  addScavengerhunt(ScavengerhuntEditor edit) async{
    this.scavengerhunts.add(edit);
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    List<String> list = prefs.getStringList("scavengerhunts");
    if(list == null) list = [];
    db.addScavengerhunt(edit);

    _updateScavengerhunts();
  }
   _updateScavengerhunts() {
    db.scavengerhunts().then((_scavengerhunts) {
      scavengerhunts = _scavengerhunts;
      notifyListeners();
    });
  }
  stopScavengerhunt() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("host_pincode");
    prefs.remove("host_admin_code");

    pincode = null;
    _adminCode = null;
    
    timer.cancel();
    GlobalKeys.navKey.currentState.pushReplacementNamed(LandingScreen.routeName);
  }
  hostScavengerhunt(ScavengerhuntEditor edit) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      final http.Response response = await http.post(
        GlobalKeys.serverUrl + "/admin/register",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, dynamic>{
          'title': edit.title,
          'maxium_time': 60 * 5 * 1000,
          'points': edit.pointsToMapForBody()
        }),
      );
      dynamic body = jsonDecode(response.body);
      if(body['result'] == null) {
        return;
      }

      _adminCode = body['result']["admin_code"];
      pincode = body['result']["pincode"];

      prefs.setInt("host_admin_code", _adminCode);
      prefs.setInt("host_pincode", pincode);
      timer = Timer.periodic(Duration(seconds: 3), (Timer t)  { _getStatus(); });
      GlobalKeys.navKey.currentState.pushReplacementNamed(HostScreen.routeName);
    } on SocketException catch(e) {
      GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
    } on NoSuchMethodError catch(e) {
      GlobalKeys.showErrorDialog("The server isn't working properly");

    }
  }
  startScavengerHunt() async {
  try {
      final http.Response response = await http.post(
        GlobalKeys.serverUrl + "/admin/startScavengerhunt",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, dynamic>{
          'admin_code': _adminCode,
          'pincode': pincode,
        }),
      );
      dynamic body = jsonDecode(response.body);

      GlobalKeys.navKey.currentState.pushReplacementNamed(HostScreen.routeName);
    } on SocketException catch(e) {
      GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
    } on NoSuchMethodError catch(e) {
      GlobalKeys.showErrorDialog("The server isn't working properly");

    }
  }
  replaceScavengerhunt(ScavengerhuntEditor edit) async{
    db.updateScavengerhuntEditor(edit);
    _updateScavengerhunts();
  }
  notfiy() {
    notifyListeners();
  }
  _getStatus() async{
   try {
    final http.Response response = await http.post(
      GlobalKeys.serverUrl + "/admin/getStatus",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          "HttpHeaders.contentTypeHeader": "application/json"

        },
        body: jsonEncode(<String, dynamic>{
          'pincode': pincode,
          'admin_code': _adminCode,
        })
      );
      
      dynamic body = jsonDecode(response.body);
      this.users = body['users'];
      this.users.sort((element, element2) => element2['score'].compareTo(element['score']));
      
      hasStarted = body['hasStarted'];
      joinable = body['joinable'];
      if(appInit) {
        appInit = false;
        GlobalKeys.navKey.currentState.pushReplacementNamed(HostScreen.routeName);
      }
     } on SocketException catch(e) {
      GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
    }

    notifyListeners();
  }
  toggleJoinable() {
   try {
    http.post(
      GlobalKeys.serverUrl + "/admin/toggleJoinable",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          "HttpHeaders.contentTypeHeader": "application/json"

        },
        body: jsonEncode(<String, dynamic>{
          'pincode': pincode,
          'admin_code': _adminCode,
        })
      );
     } on SocketException catch(e) {
      GlobalKeys.showErrorDialog("The server is offline atm, please try again later");
    }

    notifyListeners();
  }
}

class _DatabaseHandler {
  Database database;
  _DatabaseHandler() {
    initDatabase();
  }
  Future<void> initDatabase() async {
    String databases = await getDatabasesPath();
    database = await openDatabase(
      join(databases , 'scavengerhunts.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE scavengerhunts(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, points TEXT, title TEXT)",
        );
      },
      version: 1,
    );
    return;
  }
  Future<void> addScavengerhunt(ScavengerhuntEditor edit) async{
    final Database db = database;
    
    await db.insert(
      'scavengerhunts',
      {
          'title': edit.title,
          'points': jsonEncode(edit.pointsToMapForBody())
      },
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    
  }
  Future<List<ScavengerhuntEditor>> scavengerhunts() async {
    if(database == null) {
      await initDatabase();
    }

    final List<Map<String, dynamic>> maps = await database.query('scavengerhunts');

    return List.generate(maps.length, (i) {
      List<Point> points = [];
      for(int index = 0; index < jsonDecode(maps[i]['points']).length; index++) {
        List<Score> scores = [];
        for(var c = 0; c < jsonDecode(maps[i]['points'])[index]["scores"].length; c++) {
          scores.add(Score(jsonDecode(maps[i]['points'])[index]["scores"][c]["maxium"], jsonDecode(maps[i]['points'])[index]["scores"][c]["score"]));
        }
        if(jsonDecode(maps[i]['points'])[index]["type"] == "goto_location") points.add(GoToLocationEditor(title: jsonDecode(maps[i]['points'])[index]["title"], coordinate: Coordinate(jsonDecode(maps[i]['points'])[index]["coordinate"]["latitude"], jsonDecode(maps[i]['points'])[index]["coordinate"]["longitude"]), scores: scores));
        if(jsonDecode(maps[i]['points'])[index]["type"] == "RiddleLocation") points.add(RiddleLocationEditor(title: jsonDecode(maps[i]['points'])[index]["title"], riddle: jsonDecode(maps[i]['points'])[index]["riddle"], coordinate: Coordinate(jsonDecode(maps[i]['points'])[index]["coordinate"]["latitude"], jsonDecode(maps[i]['points'])[index]["coordinate"]["longitude"]), scores: scores));

      }
      return ScavengerhuntEditor(
        id: maps[i]['id'],
        title: maps[i]['title'],
        points: points,
      );
    });
  }
Future<void> updateScavengerhuntEditor(ScavengerhuntEditor edit) async {
  final db = database;

  print(edit.id);
  await db.update(
    'scavengerhunts',
      {
        'id': edit.id,
        'title': edit.title,
        'points': jsonEncode(edit.pointsToMapForBody())
      },
      where: "id = ?",
      whereArgs: [edit.id],
  );
}
  // Future<void> deleteDog(int id) async {
  //   final db = await database;

  //   await db.delete(
  //     'scavengerhunts',
  //     where: "id = ?",
  //     whereArgs: [id],
  //   );
  // }
}