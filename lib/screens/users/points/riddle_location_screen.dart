import 'dart:async';
import 'dart:convert';
import 'package:client/screens/users/points/ranking.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:client/global_keys.dart';
import 'package:client/models/points/riddle_location.dart';
import 'package:client/providers/user_provider.dart';
import 'package:client/screens/landing_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RiddleLocationScreen extends StatefulWidget {
  static String routeName = 'RiddleLocationScreen';

  @override
  _RiddleLocationScreenState createState() => _RiddleLocationScreenState();
}

class _RiddleLocationScreenState extends State<RiddleLocationScreen> with WidgetsBindingObserver{
  @override
  Widget build(BuildContext context) {
    if(!(Provider.of<UserProvider>(context).point is RiddleLocation)) GlobalKeys.navKey.currentState.pushReplacementNamed(LandingScreen.routeName);

    return DefaultTabController(
    length: 2,
    child: WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
       appBar: AppBar(
          actions: <Widget>[
              IconButton(
                icon: Icon(Icons.cancel, color: Colors.white),
                onPressed: () {
                  Provider.of<UserProvider>(context, listen: false).leaveScavengerhunt();
                },
              )
            ],
            title: Text(Provider.of<UserProvider>(context).point.title, style: TextStyle(color: Colors.white, fontSize: 28, fontFamily: 'Lato', fontWeight: FontWeight.w400 )),
          ),
          body: TabBarView(
            children: [
              Text(Provider.of<UserProvider>(context).point.riddle),
              Ranking()
            ],
          ),

          bottomNavigationBar: TabBar(
            indicatorColor: Colors.teal,
            unselectedLabelColor: Colors.black54,
            labelColor: Colors.teal,
              tabs: [
                Tab(child: Text("Riddle", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.place, size: 35)),
                Tab(child: Text("Ranking", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.people, size: 35)),
              ],
            ),
        ),
    ),
      
    );
  }
}