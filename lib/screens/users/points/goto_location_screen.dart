
import 'dart:async';

import 'package:client/screens/users/points/ranking.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:client/providers/user_provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GotoLocationScreen extends StatefulWidget {
  static String routeName = 'GotoLocationScreen';

  @override
  _GotoLocationScreenState createState() => _GotoLocationScreenState();
}

class _GotoLocationScreenState extends State<GotoLocationScreen> with WidgetsBindingObserver{
  Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.cancel, color: Colors.white),
                  onPressed: () {
                    Provider.of<UserProvider>(context, listen: false).leaveScavengerhunt();
                  },
                )
              ],
              title: Text(Provider.of<UserProvider>(context).point.title, style: TextStyle(color: Colors.white, fontSize: 28, fontFamily: 'Lato', fontWeight: FontWeight.w400 )),
            ),
            body: TabBarView(
              children: [
                Container(
                  child: GoogleMap(
                    gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                    Factory<OneSequenceGestureRecognizer>(
                    () => EagerGestureRecognizer())].toSet(),
                    mapType
                    : MapType.terrain,
                    myLocationEnabled: true,
                    myLocationButtonEnabled: true,
                    markers: Set<Marker>.of(
                    <Marker>[
                      Marker(
                        draggable: true,
                        markerId: MarkerId('Go to this marked location'),
                        position: LatLng(Provider.of<UserProvider>(context).point.coordinate.latitude, Provider.of<UserProvider>(context).point.coordinate.longtiude)
                      )
                    ]),
                    onMapCreated: _onMapCreated,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(Provider.of<UserProvider>(context).point.coordinate.latitude, Provider.of<UserProvider>(context).point.coordinate.longtiude),
                      zoom: 10
                    ),
                  ),
                ),
                Ranking()
              ],
            ),
            bottomNavigationBar: TabBar(
              indicatorColor: Colors.teal,
              unselectedLabelColor: Colors.black54,
              labelColor: Colors.teal,
              tabs: [
                Tab(child: Text("Location", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.place, size: 35)),
                Tab(child: Text("Ranking", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.people, size: 35)),
              ],
            ),
          ),
        ),
    );
    }
    void _onMapCreated(GoogleMapController controller) {
      _controller.complete(controller);
  }
  }