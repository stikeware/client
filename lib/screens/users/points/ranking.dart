import 'package:client/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Ranking extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15),
      child: Column(
         children: List.generate(Provider.of<UserProvider>(context).users.length, (index) {
           
            return Center(
              child: Card(
                child: ListTile(
                  title: Text(Provider.of<UserProvider>(context).users[index]['name']),
                  leading: Text((index + 1).toString()),
                  trailing: Text(Provider.of<UserProvider>(context).users[index]['score'].toString()) 
                )
              ),
            );
         })
      ),
    );
  }
}