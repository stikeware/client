import 'dart:async';
import 'dart:convert';
import 'package:client/screens/users/points/ranking.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:client/global_keys.dart';
import 'package:client/models/points/riddle_location.dart';
import 'package:client/providers/user_provider.dart';
import 'package:client/screens/landing_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FinishScreen extends StatefulWidget {
  static String routeName = 'FinishScreen';

  @override
  _FinishScreenState createState() => _FinishScreenState();
}

class _FinishScreenState extends State<FinishScreen> with WidgetsBindingObserver{
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),

      child: DefaultTabController(
      length: 2,
      child: Scaffold(
       appBar: AppBar(
          actions: <Widget>[
              IconButton(
                icon: Icon(Icons.cancel, color: Colors.white),
                onPressed: () {
                  Provider.of<UserProvider>(context, listen: false).leaveScavengerhunt();
                },
              )
            ],
            title: Text("Finish line", style: TextStyle(color: Colors.white, fontSize: 28, fontFamily: 'Lato', fontWeight: FontWeight.w400 )),
          ),
          body: TabBarView(
            children: [
              Text("You have finished the scavengerhunts, congrats!"),
              Ranking()
            ],
          ),

          bottomNavigationBar: TabBar(
            indicatorColor: Colors.teal,
            unselectedLabelColor: Colors.black54,
            labelColor: Colors.teal,
              tabs: [
                Tab(child: Text("Point", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.place, size: 35)),
                Tab(child: Text("Ranking", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.people, size: 35)),
              ],
            ),
        ),
        
      ),
    );
  }
}