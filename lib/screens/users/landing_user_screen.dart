import 'dart:math';

import 'package:client/global_keys.dart';
import 'package:client/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class LandingUserScreen extends StatefulWidget {
  @override
  _LandingUserScreenState createState() => _LandingUserScreenState();
}

class _LandingUserScreenState extends State<LandingUserScreen> {
  final pincodeController = TextEditingController();

  final nameController = TextEditingController();

  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    if(Provider.of<UserProvider>(context, listen: false).pincode != null) {
      FocusScope.of(context).requestFocus(FocusNode());

      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.75,
            child: TextField(
              maxLength: 15,
              controller: nameController,
              
              keyboardType: TextInputType.text,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.teal, fontFamily: "Lato", fontSize: 15),
              decoration: InputDecoration(disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal, width: 3.5)), enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal, width: 3.5)), labelText: "NAME", labelStyle: TextStyle(color: Colors.teal), counterText: ""),

            ),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
          ),
          GestureDetector(
            onTap: () async{
              if(nameController.text == ""
) {
                GlobalKeys.showErrorDialog("Enter your name");
                return;
              }
              await Provider.of<UserProvider>(context, listen: false).register(nameController.text, Random().nextDouble().toString());
            },
            child: Container(
              color: Colors.teal,
              height: MediaQuery.of(context).size.height * 0.1,
              width: MediaQuery.of(context).size.width * 0.75,
              child: Center(child: Text('ENTER', style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600),)) 
            ),
          ),
        ],
      );
    }


    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.75,
          child: TextField(
            maxLength: 5,
            controller: pincodeController,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.teal, fontFamily: "Lato", fontSize: 15),
            decoration: InputDecoration(disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal, width: 3.5)), enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal, width: 3.5)), labelText: "PINCODE", labelStyle: TextStyle(color: Colors.teal), counterText: ""),
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly
            ], //
          ),
          margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
        ),
        GestureDetector(
          onTap: () async{
            if(pincodeController.text == "") {
              GlobalKeys.showErrorDialog("Enter your pincode");
              return;
            }
            await Provider.of<UserProvider>(context, listen: false).checkPincode(int.tryParse(pincodeController.text));
            setState(() {});
          },
          child: Container(
            color: Colors.teal,
            height: MediaQuery.of(context).size.height * 0.1,
            width: MediaQuery.of(context).size.width * 0.75,
            child: Center(child: Text('ENTER', style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600),)) 
          ),
        ),
      ],
    );
  }
}