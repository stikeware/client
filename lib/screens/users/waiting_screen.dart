
import 'package:client/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WaitingScreen extends StatefulWidget{
  static String routeName = "/user/waiting_screen";
  @override
  _WaitingScreenState createState() => _WaitingScreenState();
}

class _WaitingScreenState extends State<WaitingScreen>  with WidgetsBindingObserver{
  @override
  Widget build(BuildContext context) {
    
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.cancel, color: Colors.white),
                  onPressed: () {
                    Provider.of<UserProvider>(context, listen: false).leaveScavengerhunt();
                  },
                )
          ],
          title: Text(Provider.of<UserProvider>(context).title, style: TextStyle(color: Colors.white, fontSize: 28, fontFamily: 'Lato', fontWeight: FontWeight.w400 )),
        ),
        body: GridView.count(
          crossAxisCount: 2,
            childAspectRatio: 3,
          children: List.generate(Provider.of<UserProvider>(context).users.length, (index) {
            return Center(
              child: Text(
                Provider.of<UserProvider>(context).users[index]['name'],
                style: Theme.of(context).textTheme.headline,
              ),
            );
          }),
        )
      ),
    );
  }
}