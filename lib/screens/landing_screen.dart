import 'package:client/global_keys.dart';
import 'package:client/providers/host_provider.dart';
import 'package:client/providers/user_provider.dart';
import 'package:client/screens/users/landing_user_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './host/scavengerhunt_editor/scavengerhunt_editor.dart';
import 'package:client/screens/host/landing_host_screen.dart';

class LandingScreen extends StatelessWidget {
  static String routeName = "/landingscreen";
  

  @override
  Widget build(BuildContext context) {
    Provider.of<HostProvider>(context, listen: false);
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: DefaultTabController(
        length: 2,
        
        child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.add, color: Colors.white),
                  onPressed: () {
                    GlobalKeys.navKey.currentState.pushReplacementNamed(ScavengerhuntEditorScreen.routeName);
                  },
                )
          ],

          bottom: PreferredSize(
              preferredSize: const Size.fromHeight(80),
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                alignment: Alignment.center,
                child: Text("scavengerhunt", style: TextStyle(color: Colors.white, fontSize: 27, fontFamily: 'Lato', fontWeight: FontWeight.w400 ))
            ),
          )
        ),
          bottomNavigationBar: TabBar(
            indicatorColor: Colors.teal,
            unselectedLabelColor: Colors.black54,
            labelColor: Colors.teal,
              tabs: [
                Tab(child: Text("Participate", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.directions_walk, size: 35)),
                Tab(child: Text("Organize", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.edit, size: 35)),
              ],
            ),
          body: TabBarView(
            children: [
              LandingUserScreen(),
              LandingHostScreen()
            ],
          ),
        ),
      ),
    );
  }
}