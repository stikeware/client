import 'package:flutter/material.dart';

import '../scavengerhunt_editor.dart';

class ScavengerhuntEditorForm extends StatefulWidget{
  static final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  _ScavengerhuntEditorFormState createState() => _ScavengerhuntEditorFormState();
}

class _ScavengerhuntEditorFormState extends State<ScavengerhuntEditorForm> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: ScavengerhuntEditorForm.formKey,
        child: TextFormField(
          controller: ScavengerhuntEditorScreen.titleController,
          validator: (String arg) {
              if(arg.length ==0)
                return 'The title has to have 1 character or more';
              else
                return null;
            },
          decoration: InputDecoration(
            hintText: "title",
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}