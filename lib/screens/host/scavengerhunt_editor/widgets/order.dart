import 'package:client/models/points/point.dart';
import 'package:client/providers/host_provider.dart';
import 'package:client/screens/host/scavengerhunt_editor/scavengerhunt_editor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'maps.dart';

class OrderWidget extends StatefulWidget {
  @override
  _OrderWidgetState createState() => _OrderWidgetState();
}

class _OrderWidgetState extends State<OrderWidget> {
  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [];

    for(int i = 0; i < ScavengerhuntEditorScreen.edit.points.length; i++) {
      widgets.add(
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
          key: GlobalKey(),
          alignment: FractionalOffset.topCenter,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.teal
            ),                      width: MediaQuery.of(context).size.width * 0.5,
            height: MediaQuery.of(context).size.height * 0.1,
            child: Center(child: Text(ScavengerhuntEditorScreen.edit.points[i].title, style: TextStyle(color: Colors.white, fontFamily: 'Lato'), textAlign: TextAlign.center,)),
        )
      );
    if(widgets.length == 0) return Center(child: Text("No points added yet"));
    }
    return Theme(
      data: ThemeData(
        canvasColor: Colors.transparent,
      ),
      child: ReorderableListView(
        children: widgets,
        onReorder: (int firstIndex, int afterIndex) {
            if(afterIndex ==  ScavengerhuntEditorScreen.edit.points.length) afterIndex-= 1;

            Point tmpPoint = ScavengerhuntEditorScreen.edit.points[afterIndex];
            ScavengerhuntEditorScreen.edit.points[afterIndex] = ScavengerhuntEditorScreen.edit.points[firstIndex];
            ScavengerhuntEditorScreen.edit.points[firstIndex] = tmpPoint;
            
            
            setState(() {});
            Provider.of<HostProvider>(context, listen: false).notfiy();
        },
      ),
    );
  }
}