import 'package:flutter/material.dart';

class Line extends StatelessWidget {
  const Line({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
          alignment: FractionalOffset.topCenter,
          width: MediaQuery.of(context).size.height,
          child: Container(
            height: MediaQuery.of(context).size.height * 0.1,
            width: 1,
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(color: Colors.grey, width: 5), // provides to left side
              ),
            ),
          ),
        );
  }
}