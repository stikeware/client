import 'package:flutter/material.dart';

class AddWidget extends StatelessWidget {
  final int index;
  final Function(int index, String type) callback;
  const AddWidget(this.index, this.callback);

  @override
  Widget build(BuildContext context) {
  GlobalKey _menuKey = new GlobalKey();
    return Container(
      
      alignment: FractionalOffset.topCenter,
      child: PopupMenuButton<String>(
      key: _menuKey,
        onSelected: (String type) {
          callback(index, type);
        },
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          const PopupMenuItem<String>(
            value: "riddle_location",
            child: Text('A riddle that leads to a location'),
          ),
          const PopupMenuItem<String>(
            value: "goto_location",
            child: Text('Show marked location on map'),
          ),
        ],
        child: FloatingActionButton(
        heroTag: index,
        onPressed: () {
          dynamic state = _menuKey.currentState;
          state.showButtonMenu();
        },
        child: Icon(Icons.add, color: Colors.white),
        
      ),
      )
    );
  }
}
