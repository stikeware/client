import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';
import 'package:client/models/coordinate.dart';
import 'package:search_map_place/search_map_place.dart';


import 'package:client/models/points/goto_location_editor.dart';
import 'package:client/models/points/point.dart';
import 'package:client/models/points/riddle_location_editor.dart';
import 'package:client/screens/host/scavengerhunt_editor/dialogs/goto_location_editor.dart';
import 'package:client/screens/host/scavengerhunt_editor/dialogs/riddle_location_editor.dart';
import 'package:client/screens/host/scavengerhunt_editor/scavengerhunt_editor.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/rendering.dart';

import 'package:google_maps_webservice/places.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import 'marker_generator.dart';

class CustomMap extends StatefulWidget{
  bool _onMapCreated = false;

  @override
  CustomMapState createState() => CustomMapState();
}

class CustomMapState extends State<CustomMap>{
  Set<Marker> markers = Set();
  GoogleMapController mapController;
  static bool runned = true;
  LatLng latlngCenter = LatLng(0,0);
  CustomMapState() {
    updateMarkers();
  }
  void _onMapCreated(GoogleMapController controller) {
      mapController = controller;
    updateMarkers();
    Geolocator().getCurrentPosition().then((Position latlng) {
      if(widget._onMapCreated) return;
        

      widget._onMapCreated = true;
      if(ScavengerhuntEditorScreen.latlng == LatLng(0, 0)) {
        ScavengerhuntEditorScreen.latlng = LatLng(latlng.latitude, latlng.longitude);
        controller.moveCamera(CameraUpdate.newLatLng(ScavengerhuntEditorScreen.latlng));
      }
    });
  }
  updateMarkers() async {
     this.markers = Set();
     MarkerGenerator markerGenerator = MarkerGenerator(90);
     for(int i = 0; i < ScavengerhuntEditorScreen.edit.points.length; i++) {
       BitmapDescriptor bytes = await markerGenerator.createBitmapDescriptorFromText((i + 1).toString() , Colors.teal, Colors.teal, Colors.white);
        markers.add(Marker(
          markerId: MarkerId(i.toString()), 
          icon: bytes,  
          infoWindow: InfoWindow(
            title: "Point " + (i + 1).toString(),
            snippet: "Tap to edit point",
            onTap: () {
              Point point = ScavengerhuntEditorScreen.edit.points[i];
              Dialog typeDialog = Dialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                child: Container(
                  height: 250.0,
                  width: 300.0,

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
                      Divider(),

                      FlatButton(
                        onPressed: (){
                          Navigator.of(context).pop();
                          if(ScavengerhuntEditorScreen.edit.points[i] is GoToLocationEditor) {
                            showDialog(context: context, builder: (BuildContext context) => GoToLocationEditorDialog.fromPoint(i, point)).then((v) {
                              updateMarkers();
                            });
                          }
                          if(ScavengerhuntEditorScreen.edit.points[i] is RiddleLocationEditor) {
                            showDialog(context: context, builder: (BuildContext context) => RiddleLocationEditorDialog.fromPoint(i, point)).then((v) {
                              updateMarkers();
                            });
                          }
                        },
                        child: Text('edit point', style: TextStyle(color: Colors.black87),)
                      ),
                      Divider(),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          ScavengerhuntEditorScreen.edit.points.removeAt(i);
                          updateMarkers();
                        },
                        child: Text('delete point', style: TextStyle(color: Colors.black87),)
                      ),
                      Divider(),
                    ],
                  ),
                ),
              );
              showDialog(context: context, builder: (BuildContext context) => typeDialog);
            }
          ),
          position: LatLng(ScavengerhuntEditorScreen.edit.points[i].coordinate.latitude, ScavengerhuntEditorScreen.edit.points[i].coordinate.longtiude),
          draggable: true,
          onDragEnd: (LatLng latlng) {
            ScavengerhuntEditorScreen.edit.points[i].coordinate = Coordinate(latlng.latitude, latlng.longitude);
          }
        )
      );
     }
     setState(() {});
  }
  
  @override
  Widget build(BuildContext context) {

    return Stack(
      children: [
        GoogleMap(
          gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
          Factory<OneSequenceGestureRecognizer>(
          () => EagerGestureRecognizer())].toSet(),
          mapType
          : MapType.terrain,
          myLocationEnabled: true,
          myLocationButtonEnabled: true,
          
          onTap: _handleTap,
          mapToolbarEnabled: true,
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: ScavengerhuntEditorScreen.latlng,
            zoom: 10
          ),
          markers: markers,
          onCameraMove: ((pinPosition) {latlngCenter = pinPosition.target;}),
        ),
        Center(
          child: Icon(Icons.place, color: Colors.teal, size: 30,),
        )
      ]
    );
  }
 _handleTap(LatLng point1) async {
    Widget widget ;

    LatLng point = latlngCenter;
    Dialog typeDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: 250.0,
        width: 300.0,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Different Kinds Of Points"),
            Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
            Divider(),

            FlatButton(
              onPressed: (){
                Navigator.of(context).pop();
                widget = RiddleLocationEditorDialog(ScavengerhuntEditorScreen.edit.points.length - 1, point);
              },
              child: Text('A story or riddle leads to this location ', style: TextStyle(color: Colors.black87),)
            ),
            Divider(),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
                widget = GoToLocationEditorDialog(ScavengerhuntEditorScreen.edit.points.length - 1, point);

              },
              child: Text('Participant has to travel to this location', style: TextStyle(color: Colors.black87),)
            ),
            Divider(),
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (BuildContext context) => typeDialog).then((v) {
      if(widget == null) return;
      showDialog(
        context: context,
        builder: (BuildContext context) => widget,
      ).then((val) {
        updateMarkers();
      });
    });
  }
  @override
  initState() {
    super.initState();
    updateMarkers();

  }

}