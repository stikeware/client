import 'dart:async';


import 'package:client/providers/host_provider.dart';
import 'package:client/screens/host/scavengerhunt_editor/widgets/form.dart';
import 'package:client/screens/host/scavengerhunt_editor/widgets/maps.dart';
import 'package:client/screens/host/scavengerhunt_editor/widgets/order.dart';
import 'package:flutter/foundation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:client/models/scavengerhunt_editor.dart';
import 'package:client/screens/landing_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:client/models/points/point.dart';
import 'package:provider/provider.dart';

import '../../../global_keys.dart';





class ScavengerhuntEditorScreen extends StatefulWidget {
  static String routeName = '/route/edit';
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static LatLng latlng = LatLng(0, 0);
  static int index = 0;
  static final titleController = TextEditingController();
  bool copied = false;
  static ScavengerhuntEditor edit = ScavengerhuntEditor(points: [], title: "");

  @override
  _ScavengerhuntEditorScreenState createState() => _ScavengerhuntEditorScreenState();
}

class _ScavengerhuntEditorScreenState extends State<ScavengerhuntEditorScreen> {
  @override
  Widget build(BuildContext context) {
  try { 

    if(ModalRoute.of(context).settings.arguments is ScavengerhuntEditor) {
      ScavengerhuntEditorScreen.edit = ModalRoute.of(context).settings.arguments;
      ScavengerhuntEditorScreen.titleController.text = ScavengerhuntEditorScreen.edit.title;
      widget.copied = true;
    }

  } on FormatException catch(e) {

  } on NoSuchMethodError catch(e) {

  }
    return DefaultTabController(
      length: 3,
      
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.save, color: Colors.white),
                onPressed: () {
                if(!ScavengerhuntEditorForm.formKey.currentState.validate() || ScavengerhuntEditorScreen.edit.points.length == 0) {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Do you want to throw away this concept"),
                        content: Text("The scavengerhunt has to have a title and has to have 1 or more points"),
                        actions: [
                          FlatButton(
                            child: Text("No"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text("Yes"),
                            onPressed: () {
                              ScavengerhuntEditorScreen.edit = ScavengerhuntEditor(points: [], title: "");
                              Navigator.of(context).pop();
                              GlobalKeys.navKey.currentState.pushReplacementNamed(LandingScreen.routeName);
                            },
                          ),
                        ],
                      );
                    },
                  );
                } else {
                  ScavengerhuntEditorScreen.edit.title = ScavengerhuntEditorScreen.titleController.text;
                  if(!widget.copied) {
                    Provider.of<HostProvider>(context, listen: false).addScavengerhunt(ScavengerhuntEditorScreen.edit);
                  } else {
                    print(ScavengerhuntEditorScreen.index);
                    Provider.of<HostProvider>(context, listen: false).replaceScavengerhunt(ScavengerhuntEditorScreen.edit);
                  }

                  ScavengerhuntEditorScreen.edit = ScavengerhuntEditor(points: [], title: "");
                  GlobalKeys.navKey.currentState.pushReplacementNamed(LandingScreen.routeName);
                }
                },
              ),
              IconButton(
                icon: Icon(Icons.play_arrow, color: Colors.white),
                onPressed: () {
                if(!ScavengerhuntEditorForm.formKey.currentState.validate() || ScavengerhuntEditorScreen.edit.points.length == 0) {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Invalid scavengerhunt"),
                        content: Text("The scavengerhunt has to have a title and has to have 1 or more points"),
                        actions: [
                          FlatButton(
                            child: Text("Okay"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                } else {
                  ScavengerhuntEditorScreen.edit.title = ScavengerhuntEditorScreen.titleController.text;
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Host and save scavengerhunt"),
                        actions: [
                          FlatButton(
                            child: Text("No"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text("Yes"),
                            onPressed: () {
                              Navigator.of(context).pop();
                              if(!widget.copied) {
                                Provider.of<HostProvider>(context, listen: false).addScavengerhunt(ScavengerhuntEditorScreen.edit);
                              } else {
                                print(ScavengerhuntEditorScreen.index);
                                Provider.of<HostProvider>(context, listen: false).replaceScavengerhunt(ScavengerhuntEditorScreen.edit);
                              }                              Provider.of<HostProvider>(context, listen: false).hostScavengerhunt(ScavengerhuntEditorScreen.edit);
                              ScavengerhuntEditorScreen.edit = ScavengerhuntEditor(points: [], title: "");
                            },
                          ),
                        ],
                      );
                    },
                  );
                }
                },
              )
            ],
            automaticallyImplyLeading: false,
            iconTheme: IconThemeData(color: Colors.white),
            title: Text("Create your own scavengerhunt",
            style: TextStyle(color: Colors.white,
            fontFamily: 'Lato',
            
            ))),
          bottomNavigationBar: TabBar(
            indicatorColor: Colors.teal,
            unselectedLabelColor: Colors.black54,
            labelColor: Colors.teal,
              tabs: [
                Tab(child: Text("Settings", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.directions_walk, size: 35)),
                Tab(child: Text("Locations", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.place, size: 35)),
                Tab(child: Text("Order", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.edit, size: 35)),
              ],
            ),
          body: TabBarView(
            children: [
              ScavengerhuntEditorForm(),
              MyHomePage(),
              OrderWidget()
            ],
          ),
        ),
      )
    );
  }
  Future<bool> _willPopCallback() => Future.value(true);
} 