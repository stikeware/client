import 'package:client/models/coordinate.dart';
import 'package:client/models/points/goto_location_editor.dart';
import 'package:client/models/points/riddle_location_editor.dart';
import 'package:client/models/score.dart';
import 'package:client/screens/host/scavengerhunt_editor/scavengerhunt_editor.dart';
import 'package:client/screens/host/scavengerhunt_editor/widgets/maps.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoToLocationEditorDialog extends StatefulWidget {
  
  int index;  
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static final GlobalKey<FormState> _scoreFormKey = GlobalKey<FormState>();

  final titleController = TextEditingController();
  final mintuesController = TextEditingController();
  final scoreController = TextEditingController();
  LatLng latlng;
  bool copied = false;
  List<Score> scores = [];

  GoToLocationEditorDialog(this.index, LatLng latlng) {
   this.index = index;
   this.latlng = latlng;
  }
  GoToLocationEditorDialog.fromPoint(int index, GoToLocationEditor point) {
      titleController.text = point.title;
      scores = point.scores;
      latlng = LatLng(point.coordinate.latitude, point.coordinate.longtiude);
      this.index = index;
      copied = true;
  }
  @override
  _GoToLocationEditorDialogState createState() => _GoToLocationEditorDialogState();
}

class _GoToLocationEditorDialogState extends State<GoToLocationEditorDialog> {

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
      ),
      
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext ctx) {
    return Container(
      height: MediaQuery.of(ctx).size.height * 0.6,
      margin: EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        children: <Widget>[

          ListTile(
            leading: const Icon(Icons.title),
            title: Form(
              key: GoToLocationEditorDialog._formKey,
              child: Column(
                children: [
                  TextFormField(
                    validator: (String arg) {
                        if(widget.scores.length == 0) {
                          widget.scores.add(Score(0, 0));
                        }
                        if(arg.length ==0)
                          return 'The title has to have 1 character or more';
                        else {
                          return null;
                        }
                      },
                    controller: widget.titleController,
                    decoration: InputDecoration(
                      hintText: "title",
                    ),
                ),
                ]
              )
            ),
          ),
          Container(
            height: MediaQuery.of(ctx).size.height * 0.9 * 0.35,
            child: ListView(
              children: List.generate(widget.scores.length + 1 , (int index){
                if(index == 0) {
                  return Card(  
                    child: Padding(
                      padding: EdgeInsets.all(7.0),
                      child: ListTile(
                        title: Form(
                          key: GoToLocationEditorDialog._scoreFormKey,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: TextFormField(
                                  validator: (String arg) {
                                    if(arg.length ==0)
                                      return '1 or minutes score are required';
                                    else
                                      return null;
                                  },
                                  keyboardType: TextInputType.number,
                                  controller: widget.mintuesController,
                                  decoration: InputDecoration(
                                    hintText: "minutes",
                                  ),
                                ),
                              ),
                              Container(margin: EdgeInsets.fromLTRB(0, 0, 12, 0),),
                              Expanded(
                                child: TextFormField(
                                  validator: (String arg) {
                                    
                                    if(arg.length ==0)
                                      return '1 or more score are required';
                                    else
                                      return null;
                                  },
                                  keyboardType: TextInputType.number,
                                  controller: widget.scoreController,
                                  decoration: InputDecoration(
                                    hintText: "score",
                                  ),
                                ),
                              ),

                            ],
                          ),
                        ),
                        trailing:
                          FlatButton(
                            color: Colors.green,
                            textColor: Colors.white,
                            disabledColor: Colors.grey,
                            disabledTextColor: Colors.black,
                            padding: EdgeInsets.all(8.0),
                            splashColor: Colors.teal,
                            onPressed: () {
                              if(!GoToLocationEditorDialog._scoreFormKey.currentState.validate()) return;
                              widget.scores.add(Score(int.parse(widget.mintuesController.text), int.parse(widget.scoreController.text)));
                              setState(() {});
                            },
                            child: Text(
                              "+",
                            ),
                          )
                      ),
                    ),
                  );
                }
                return Card(  
                  child: Padding(
                    padding: EdgeInsets.all(7.0),
                    child: ListTile(
                      title: Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(widget.scores[index - 1].time.toString())
                            ),
                            Container(margin: EdgeInsets.fromLTRB(0, 0, 12,
                             0),),
                            Expanded(
                              child: Text(widget.scores[index - 1].score.toString())

                            ),

                          ],
                        ),
                        trailing:
                          Container(
                            width: 45,
                            child: FlatButton(
                              color: Colors.red,
                              textColor: Colors.white,
                              disabledColor: Colors.grey,
                              disabledTextColor: Colors.black,
                              padding: EdgeInsets.all(8.0),
                              splashColor: Colors.teal,
                              onPressed: () {
                                widget.scores.removeAt(index - 1);
                                setState(() {});
                              },
                              child: Text(
                                "-",
                              ),
                            ),
                          )
                    ),
                  ),
                );
              })
            )
          ),
          GestureDetector(
            onTap: () {
              if(!GoToLocationEditorDialog._formKey.currentState.validate()) return;
              if(widget.copied) {
                ScavengerhuntEditorScreen.edit.points[widget.index] = GoToLocationEditor(title: widget.titleController.text, coordinate: Coordinate(widget.latlng.latitude, widget.latlng.longitude), scores: widget.scores);
                Navigator.pop(ctx);

                return;
              }

              ScavengerhuntEditorScreen.edit.points.add(GoToLocationEditor(title: widget.titleController.text, coordinate: Coordinate(widget.latlng.latitude, widget.latlng.longitude),scores: widget.scores));
              Navigator.pop(ctx);

            },
            child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.teal,
            ),
            margin: EdgeInsets.fromLTRB(0, 15, 0, 15),
            alignment: FractionalOffset.topCenter,
            width: MediaQuery.of(ctx).size.width * 0.5,
            height: MediaQuery.of(ctx).size.height * 0.1,
            child: Center(child: Text("ENTER", style: TextStyle(color: Colors.white, fontFamily: 'Lato'))),
            ),
          ),
        ]
      ),
    );
  }
}