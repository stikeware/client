import 'dart:convert';

import 'package:client/global_keys.dart';
import 'package:client/models/scavengerhunt_editor.dart';
import 'package:client/providers/host_handler.dart';
import 'package:client/providers/host_provider.dart';
import 'package:client/screens/host/scavengerhunt_editor/scavengerhunt_editor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LandingHostScreen extends StatefulWidget {
 final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool hasRunned = false;
  LandingHostScreen({Key key}) : super(key: key);

  @override
  _LandingHostScreenState createState() => _LandingHostScreenState();
}

class _LandingHostScreenState extends State<LandingHostScreen> {
  @override
  Widget build(BuildContext context) {
    int index = 0;
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,20,0,0),
      child: ListView(
          
          children: Provider.of<HostProvider>(context).scavengerhunts.map((ScavengerhuntEditor edit) {
            index++;
            return GestureDetector(
              onTap: () {
                ScavengerhuntEditorScreen.index = index -1;
                GlobalKeys.navKey.currentState.pushReplacementNamed(ScavengerhuntEditorScreen.routeName, arguments: edit);
                //Provider.of<HostProvider>(context, listen: false).hostScavengerhunt(edit);
              },
              child: Card(
                child: ListTile(
                  leading: Icon(Icons.directions_car, size: 50, color: Colors.teal),
                  title: Text(edit.title, style: TextStyle(color: Colors.black, fontSize: 16, fontFamily: 'Lato', fontWeight: FontWeight.w500,)),
                ),
              ),
            );
          }
        ).toList()
      ),
    );

  }
}