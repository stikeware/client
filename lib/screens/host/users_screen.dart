import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:client/providers/host_handler.dart';
import 'package:client/providers/host_provider.dart';
import 'package:client/providers/user_provider.dart';
import 'package:http/http.dart' as http;
import 'package:client/models/metadata.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../global_keys.dart';

class UserScreen extends StatefulWidget{
  @override
  _UserScreenState createState() => _UserScreenState();


}

class _UserScreenState extends State<UserScreen>  {
  Timer timer;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(7.5),
      child: Column(
         children: List.generate(Provider.of<HostProvider>(context).users.length, (index) {
           
            return Center(
              child: Card(
                child: ListTile(
                  title: Text(Provider.of<HostProvider>(context).users[index]['name']),
                  leading: Text((index + 1).toString()),
                  trailing: Text(Provider.of<HostProvider>(context).users[index]['score'].toString()) 
                )
              ),
            );
         })
      ),
    );
  }
}