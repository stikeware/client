import 'package:client/providers/host_handler.dart';
import 'package:client/providers/host_provider.dart';
import 'package:client/screens/host/setting_screen.dart';
import 'package:client/screens/host/users_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HostScreen extends StatelessWidget {
  static String routeName = "/hostScreen";
  static bool hasRunned = false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: DefaultTabController(
        length: 2,
        
        child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.cancel, color: Colors.white),
                  onPressed: () {
                    Provider.of<HostProvider>(context, listen: false).stopScavengerhunt();
                  },
                )
          ],

          bottom: PreferredSize(
              preferredSize: const Size.fromHeight(80),
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                alignment: Alignment.center,
                child: Text(Provider.of<HostProvider>(context).pincode.toString(), style: TextStyle(color: Colors.white, fontSize: 27, fontFamily: 'Lato', fontWeight: FontWeight.w400 ))
            ),
          )
        ),
          bottomNavigationBar: TabBar(
            indicatorColor: Colors.teal,
            unselectedLabelColor: Colors.black54,
            labelColor: Colors.teal,
              tabs: [
                Tab(child: Text("Manage", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.directions_walk, size: 35)),
                Tab(child: Text("Users", style: TextStyle(fontFamily: 'Lato', fontSize: 15)), icon: Icon(Icons.edit, size: 35)),
              ],
            ),
          body: TabBarView(
            children: [
              UserScreen(),
              SettingScreen(),

            ],
          ),
        ),
      ),
    );
  }
}