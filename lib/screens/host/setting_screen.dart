import 'package:client/providers/host_handler.dart';
import 'package:client/providers/host_provider.dart';
import 'package:client/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    Widget hasStarted = GestureDetector(
      onTap: () async{
        Provider.of<HostProvider>(context, listen: false).startScavengerHunt();
      },
      child: Center(
        child: Container(
          color: Colors.teal,
          height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width * 0.9,
          child: Center(child: Text('START SCAVENGERHUNT', style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600),)) 
        ),
      ),
    );
    if(Provider.of<HostProvider>(context).hasStarted) {
      hasStarted = Text("Scavengerhunt has started!");
    }
    return Column(
      children: <Widget>[
        SwitchListTile(
          value: Provider.of<HostProvider>(context, listen: false).joinable,
          title: Text("Scavengerhunt joinable"),
          subtitle: Text("Participants can join the scavengerhunt"),
          onChanged: (value) {
            Provider.of<HostProvider>(context, listen: false).joinable = !Provider.of<HostProvider>(context, listen: false).joinable;
            Provider.of<HostProvider>(context, listen: false).toggleJoinable();

          },
        ),
        Divider(color: Colors.teal, thickness: 4),
        hasStarted
      ]
    );
  }
}