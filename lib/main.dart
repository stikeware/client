import 'package:client/models/scavengerhunt_editor.dart';
import 'package:client/providers/host_handler.dart';
import 'package:client/providers/host_provider.dart';
import 'package:client/providers/user_provider.dart';
import 'package:client/screens/host/host_screen.dart';
import 'package:client/screens/landing_screen.dart';
import 'package:client/screens/users/points/goto_location_screen.dart';
import 'package:client/screens/users/points/riddle_location_screen.dart';
import 'package:client/screens/users/points/finish_screen.dart';

import 'package:client/screens/users/waiting_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'global_keys.dart';
import 'screens/host/scavengerhunt_editor/scavengerhunt_editor.dart';

void main() {
  runApp(MyApp());
} 

class MyApp extends StatelessWidget {
final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  // This widget is the root of your application.
 @override
  Widget build(BuildContext context) { 
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    return MultiProvider(
      providers: [
          ChangeNotifierProvider(create: (_) => UserProvider()),
          ChangeNotifierProvider(create: (_) => HostProvider()),
      ],
      child: MaterialApp(
        title: 'scavengerhunts',
        navigatorKey: GlobalKeys.navKey,

        theme: ThemeData(
          primarySwatch: Colors.teal,
          fontFamily: "Lato",
        ),
        home: LandingScreen(),
        onGenerateRoute: _getRoute,
      )
    );
  }
  Route _getRoute(RouteSettings settings) {
    if(settings.name == LandingScreen.routeName ) return _buildRoute(settings, LandingScreen());
    if(settings.name == WaitingScreen.routeName ) return _buildRoute(settings, WaitingScreen());
    if(settings.name == ScavengerhuntEditorScreen.routeName ) return _buildRoute(settings, ScavengerhuntEditorScreen());
    if(settings.name == RiddleLocationScreen.routeName ) return _buildRoute(settings, RiddleLocationScreen());
    if(settings.name == FinishScreen.routeName ) return _buildRoute(settings, FinishScreen());
    if(settings.name == HostScreen.routeName ) return _buildRoute(settings, HostScreen());
    if(settings.name == GotoLocationScreen.routeName ) return _buildRoute(settings, GotoLocationScreen());

  }

  MaterialPageRoute _buildRoute(RouteSettings settings, Widget builder){
    return new MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => builder,
    );
  }
  
}
