
import "dart:convert";

import "package:client/models/coordinate.dart";
import 'package:client/models/points/goto_location_editor.dart';
import "package:client/models/points/point.dart";
import "package:client/models/points/riddle_location_editor.dart";
import "package:client/models/score.dart";

class ScavengerhuntEditor {
  String title;
  int id;
  List<Point> points = [];
  
  ScavengerhuntEditor({ this.title,this.points, this.id});

  ScavengerhuntEditor.fromEncodedDynamic(String encodedDynamic) {
    dynamic json = jsonDecode(encodedDynamic);
    this.title = json["title"];
    for(int i = 0; i < json["points"].length; i++) {
      List<Score> scores = [];
      for(var c = 0; c < jsonDecode(json["points"][i]["scores"]).length; c++) {
        scores.add(Score(jsonDecode(json["points"][i]["scores"])[c]["maxium"], jsonDecode(json["points"][i]["scores"])[c]["score"]));
      }
      if(json["points"][i]["type"] == "RiddleLocation") this.points.add(RiddleLocationEditor(title: json["points"][i]["title"], riddle: json["points"][i]["riddle"], coordinate: Coordinate(json["points"][i]["coordinate"]["latitude"], json["points"][i]["coordinate"]["latitude"]), scores: scores));
      if(json["points"][i]["type"] == "goto_location") this.points.add(GoToLocationEditor(title: json["points"][i]["title"], coordinate: Coordinate(json["points"][i]["coordinate"]["latitude"], json["points"][i]["coordinate"]["latitude"]), scores: scores));

    }
  }
  List<dynamic> pointsToMapForBody() {
    List<dynamic> points = [];

    for(int i = 0; i < this.points.length; i++) {
      
      List<dynamic> scores = [];
      for(int c = 0; c < this.points[i].scores.length; c++) {
        scores.add({"score": this.points[i].scores[c].score, "maxium": this.points[i].scores[c].time});
      }
      if(this.points[i] is GoToLocationEditor) points.add({"type": "goto_location", "title": this.points[i].title, "scores": scores, "coordinate": {"latitude": this.points[i].coordinate.latitude, "longitude": this.points[i].coordinate.longtiude}});
      if(this.points[i] is RiddleLocationEditor) points.add({"type": "RiddleLocation", "title": this.points[i].title, "riddle": this.points[i].riddle, "scores": scores, "coordinate": {"latitude": this.points[i].coordinate.latitude, "longitude": this.points[i].coordinate.longtiude}});
    }
    return points;
  }
  List<dynamic> pointsToMap() {
    List<dynamic> points = [];

    for(int i = 0; i < this.points.length; i++) {
      
      String scores = "[";
      for(int c = 0; c < this.points[i].scores.length; c++) {
        scores += ('{"score":' + (this.points[i].scores[c].score).toString() + ',"maxium":' + (this.points[i].scores[c].time * 60 * 1000).toString() + "},");
      }
      if(this.points[i] is GoToLocationEditor) points.add({"type": "goto_location", "title": this.points[i].title, "scores": scores.substring(0, scores.length -1) + "]", "coordinate": {"latitude": this.points[i].coordinate.latitude, "longitude": this.points[i].coordinate.longtiude}});
      if(this.points[i] is RiddleLocationEditor) points.add({"type": "RiddleLocation", "title": this.points[i].title, "riddle": this.points[i].riddle, "scores": scores.substring(0, scores.length -1) + "]", "coordinate": {"latitude": this.points[i].coordinate.latitude, "longitude": this.points[i].coordinate.longtiude}});
    }
    return points;
  }
}
