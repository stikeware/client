import 'package:client/models/points/point.dart';

import 'metadata.dart';

class Scavengerhunt {
  final int pincode;

  String title;
  Point currentPoint; 
  List<Point> previousPoints = [];
  Metadata metadata;

  Scavengerhunt(this.pincode);
}