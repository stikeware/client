import 'package:client/models/coordinate.dart';
import 'package:client/models/points/point.dart';

import '../score.dart';

class GoToLocationEditor extends Point {
  String title;
  Coordinate coordinate;
  String type = "point";
  List<Score> scores;

  GoToLocationEditor({ this.title, this.coordinate, this.scores});
  
}