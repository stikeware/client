import 'package:client/models/coordinate.dart';
import 'package:client/models/points/point.dart';

class GoToLocation extends Point {
  String title;
  Coordinate coordinate;
  String type = "goto_location";

  GoToLocation({ this.title, this.coordinate});
  
}