import 'package:client/models/coordinate.dart';

import '../score.dart';

class Point {
  final String title;
  String type = "point";
  Coordinate coordinate;
  String riddle;

  List<Score> scores;

  Point({ this.title });

}