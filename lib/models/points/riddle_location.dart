import 'package:client/models/coordinate.dart';
import 'package:client/models/points/point.dart';

class RiddleLocation extends Point {
  String title;
  String riddle;

  String type = "riddle_location";

  RiddleLocation({ this.title, this.riddle });
  
}