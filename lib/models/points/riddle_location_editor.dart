import 'package:client/models/coordinate.dart';
import 'package:client/models/points/point.dart';

import '../score.dart';

class RiddleLocationEditor extends Point {
  String title;
  String riddle;
  Coordinate coordinate;
  String type = "riddle_location";
    List<Score> scores;

  RiddleLocationEditor({ this.title, this.riddle, this.coordinate, this.scores});
  
}